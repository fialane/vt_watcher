const fs = require("fs");
const net = require("net");
const xml2js = require("xml2js");
const merge = require("deepmerge");

const appName = "FACEIT Media Ad Tally";
const appVersion = "1.0.0b";

var rxBuffer = Buffer.from("");
var state = {}; //This is the object that get's filled when a packet is received

try {
  let xml = `
	<ETP>
		<Authentication-Request>
			<Protocol>GV Ethernet Tally</Protocol>
			<ProtocolVersion>2.0</ProtocolVersion>
			<AppName>${appName}</AppName>
			<AppVersion>${appVersion}</AppVersion>
			<Suite>All</Suite>
		</Authentication-Request>
	</ETP>`;

  let authPacket = xml2js.parseString(xml, function(err, result) {
    console.log(result);
  });

  //connect to switcher
  var client = new net.Socket();
  client.connect(
    2012,
    "172.23.0.157",
    function(err) {
      console.log("Connected");
      client.write(xml); //our xml object goes here
    }
  );

  client.on("data", function(data) {
    console.log("---Received a Packet---");
    //console.log(String(data));
    let rxb = Buffer.from(String(data));
    rxBuffer = Buffer.concat([rxBuffer, rxb]);
    //check if last part is \n
    if (rxBuffer.lastIndexOf(`\n`) == rxBuffer.length - 1) {
      let bufString = rxBuffer.toString();
      let props = bufString.split("<ETP>");

      for (let prop = 0; prop < props.length; prop++) {
        props[prop] = props[prop].replace(`<ETP>`, "");
        props[prop] = props[prop].replace(`</ETP>`, "");
        let parser = new xml2js.Parser();
        console.log(props[prop]);
        parser.parseString(props[prop], function(err, result) {
          if (err) {
            console.log(err);
          }
          if (result) {
            for (var k = 0; k < Object.keys(result).length; k++) {
              let key = Object.keys(result)[k];
              if (!state[key]) state[key] = {};
              state[key] = merge(state[key], result); //JSON.parse(JSON.stringify(result));
            }
          }
        });
      }
      rxBuffer = Buffer.from("");
      fs.writeFile("etr.json", JSON.stringify(state), () => {
        console.log("written");
      });
    }
  });

  client.on("close", function() {
    console.log("Connection Closed");
  });

  client.on("error", function(err) {
    //retry connection on error
    console.log(err);
    client.connect(
      2012,
      "172.23.0.157",
      function(err) {
        setTimeout(function() {
          console.log("Connected");
          client.write(xml);
        }, 2);
      }
    );
  });
} catch (err) {
  console.log(err);
}
