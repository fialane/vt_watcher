var osc = require("osc");
var ATEM = require("applest-atem");
var request = require("request");
const OBSWebSocket = require("obs-websocket-js");

var prevAdvert = "";
var length = 15;
var framerate = 59.94;
var prevFrame = 5; //initialise to 100 to get a signal from the first clip
var newClip = false;
var isAdv = false;
var isLive = false;
var adv = null;
var vt = 3;
var type = "NONE";
var complete = false;

const rtmp_delay = 1300;
const start_url =
  "http://naw-nginx.faceit.media/control/record/start?app=PGM&name=&rec=auto";
const stop_url =
  "http://naw-nginx.faceit.media/control/record/stop?app=PGM&name=&rec=auto";

/*
const obs1 = new OBSWebSocket();
const obs2 = new OBSWebSocket();
obs1.connect({ address: "172.23.0.149:4444", password: "ENC7" });
obs2.connect({ address: "172.23.0.150:4444", password: "ENC8" });
*/
var atem = new ATEM();
atem.connect("172.23.0.21");
atem.on("connect", () => {
  console.log("ATEM Connected");
}); // atem

// Create an osc.js UDP Port listening on port 57121.
var udpPort = new osc.UDPPort({
  localAddress: "0.0.0.0",
  localPort: 5253
});
/*
atem.on("stateChanged", function(state) {
  if (atem.state.video.ME[0].programInput != vt) {
    
    obs1.send("SetCurrentScene", { "scene-name": "pgm" });
    obs2.send("SetCurrentScene", { "scene-name": "pgm" });

    prevAdvert = "";
  } // if
});
*/

// Listen for incoming OSC bundles.
udpPort.on("bundle", function(oscBundle, timeTag, info) {
  try {
    //console.log(JSON.stringify(oscBundle));
    //console.log("ENDSTART");
    newClip = false;
    isAdv = false;
    isLive = false;
    var packets = oscBundle.packets;
    for (var i = 0; i < packets.length; i++) {
      if (packets[i].address.startsWith("/channel/1/")) {
        var pak = packets[i];
        if (pak.address.includes("file/path")) {
          if (pak.args[0]) {
            adv = pak.args[0];
            adv = adv.replace(/^.*[\\\/]/, "");
            if (pak.args[0].includes("_start")) {
              type = "START";
              isAdv = true;
            } // if
            else if (pak.args[0].includes("_stop")) {
              type = "STOP";
              isAdv = true;
            } // if
            else {
              type = "NONE";
              isAdv = false;
            } // else
          } // if
        } // if
        /*
        if (pak.address.includes("/layer/11/file/path")) {
          if (pak.args[0].includes("START")) {
            adv = pak.args[0];
            adv = adv.replace(/^.*[\\\/]/, "");
            isAdv = true;
          } else {
            obs1.send("SetCurrentScene", { "scene-name": "pgm" });
            obs2.send("SetCurrentScene", { "scene-name": "pgm" });
          } // else
          /*
          if (pak.args[0].low < parseInt(prevFrame)) {
            console.log("its a new clip");
            newClip = true;
          } // if
        }*/
        if (pak.address.includes("/frame")) {
          prevFrame = pak.args[0].low;
          length = pak.args[1].low;
          complete = length - prevFrame < 30;
        } //update prevFrame

        //ping off a http request here atem and http
        if (atem.state && atem.state.video.ME[0].programInput == vt) {
          isLive = true;
        } // if
      } // if
    } // for
    if (
      prevAdvert !== adv &&
      isAdv &&
      isLive &&
      ((type == "STOP" && complete) || type != "STOP")
    ) {
      console.log(
        `IS ADV:${adv} ${type} LIVE: ${isLive} COMPLETE: ${complete} PREV: ${prevAdvert}`
      );
      if (type == "STOP") {
        setTimeout(() => {
          console.log(`DOING: ${stop_url}`);
          request(
            {
              url: stop_url,
              method: "GET"
            },
            function(error, resp, body) {
              console.log(error);
              console.log(body);
            }
          );
        }, rtmp_delay);
      } else if (type == "START") {
        setTimeout(() => {
          console.log(`DOING: ${start_url}`);
          request(
            {
              url: start_url,
              method: "GET"
            },
            function(error, resp, body) {
              console.log(error);
              console.log(body);
            }
          );
        }, rtmp_delay);
      } // if

      prevAdvert = adv;
      complete = false;
      type = "NONE";

      /*
      //fire off http request and atem
      var wayPoint = new Object();
      wayPoint.assetName = adv;
      wayPoint.channelId = "faceittv";
      wayPoint.isTest = false;
      wayPoint.duration = Math.round(length / framerate);
      console.log(wayPoint);
      var url =
        "https://api.waypointmedia.com/events?apiKey=TjE0Qi1zHY_nQOEU4KqROIwuTZbBGH0G";
      request(
        {
          url: url,
          method: "POST",
          headers: {
            "content-type": "application/json"
          },
          json: wayPoint
        },
        function(error, resp, body) {
          console.log(error);
          console.log(body);
        }
      );
      console.log("Adblocking");
      obs1.send("SetCurrentScene", { "scene-name": "adblock" });
      obs2.send("SetCurrentScene", { "scene-name": "adblock" });
      */
    } // if
  } catch (err) {
    console.log(err);
  } //. catch
});

// Open the socket.
udpPort.open();
